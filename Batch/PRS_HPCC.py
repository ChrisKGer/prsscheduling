import pandas as pd
import time
import math
import numpy as np
import random
import sys #error outputs
import pickle

#######
#Global variable
globScheduleDays = 5
globScheduleSlots = 10

#Classes
#########################################################################
class group:

    instlist = [] #Making class iterable
    numGroup = 0

    def __init__(self,numStudents,grade,hours, scheduleDays, scheduleSlots):
        self.Id = group.numGroup
        group.numGroup += 1

        self.numStudents = numStudents
        self.grade = grade
        self.hours = hours
        self.busy = np.zeros( (scheduleSlots, scheduleDays) )

        group.instlist.append(self)

class room:

    instlist = [] #Making class iterable
    numRoom = 0

    def __init__(self,capacity, equipment, scheduleDays, scheduleSlots):
        self.Id = room.numRoom
        room.numRoom += 1

        self.capacity = capacity
        self.equipment = equipment
        self.busy = np.zeros( (scheduleSlots, scheduleDays) )

        room.instlist.append(self)

class teacher:


    instlist = [] #Making class iterable
    numTeachers = 0

    def __init__(self,first,last,years, subjects, workhours, scheduleDays,scheduleSlots):
        self.Id = teacher.numTeachers # Each teacher gets own ID
        teacher.numTeachers += 1

        self.first = first
        self.last = last
        self.years = years
        self.subjects = subjects
        self.workhours = workhours
        self.busy = np.zeros( (scheduleSlots, scheduleDays) )

        teacher.instlist.append(self)

#Functions
#########################################################################
def breed_Next(scheduleArray, numKeep,numMutate ,numNew, mutateTH, group,teacher, room, numDays, numSlots):
    numRooms = len(room.instlist)
    numTeachers = len(teacher.instlist)
    numObs = len(scheduleArray)
    #Keep Top Observations
    newArray = []
    newArray.extend(scheduleArray[0:numKeep])

    #CrossOver all successful ones with each other
    for i in range(numKeep):
        for j in range(numKeep):
            if i != j :
                newArray.append(crossover(scheduleArray[i], scheduleArray[j]))

    #Mutate each of the successful ones (strongly and not so strongly)
    for i in range(int(numKeep*numMutate/2)):
        workExample = scheduleArray[int(i/numMutate)].copy() #So we dont work on the actual copy
        newArray.append(mutate(workExample,mutateTH, numDays, numSlots, numRooms, numTeachers))
        newArray.append(mutate(workExample,mutateTH*2, numDays, numSlots, numRooms, numTeachers))

    #Create new Observations
    newArray.extend(create_NewGen(numNew,group, teacher, room, numDays, numSlots))

    #Cross-Over random ones to fill up array
    numCurrent = len(newArray)
    for i in range(numCurrent,numObs):
        X = np.random.randint(numCurrent)
        Y = np.random.randint(numCurrent)
        newArray.append(crossover(newArray[X],newArray[Y]))

    return newArray

def create_NewGen(numSchedules,group, teacher, room, globScheduleDays, globScheduleSlots):
    SchedArr = []

    for i in range(numSchedules):
        tempArr = create_Schedule(group, teacher, room, globScheduleDays, globScheduleSlots)
        SchedArr.append(tempArr)

    return SchedArr

def create_Schedule(group, teacher, room, scheduleDays, scheduleSlots):# Sort rooms

    #Reset business of all instances
    for item in group.instlist:
        item.busy = np.zeros( (scheduleSlots, scheduleDays) )
    for item in teacher.instlist:
        item.busy=np.zeros( (scheduleSlots, scheduleDays) )
    for item in room.instlist:
        item.busy=np.zeros( (scheduleSlots, scheduleDays) )

    normRooms = []
    sportRooms = []
    itRooms = []
    labRooms = []
    musicRooms = []

    #Split all rooms in their respective types
    for roomObj in room.instlist:
        if roomObj.equipment == 1:
            normRooms.append(roomObj)
        elif roomObj.equipment == 2:
            sportRooms.append(roomObj)
        elif roomObj.equipment == 3:
            itRooms.append(roomObj)
        elif roomObj.equipment == 4:
            labRooms.append(roomObj)
        elif roomObj.equipment == 5:
            musicRooms.append(roomObj)

    #Initiate dataframe in which results will be stored
    ScheduleItems = pd.DataFrame(columns=["GroupID","Subject","Day","Slot","RoomID","First", "Last ","TeacherID"])

    #Find suitable match
    for groupObj in group.instlist: # Every Class
        for SubjectIndex, hourObj in enumerate(groupObj.hours): #Every subject of every class

            #Find suitable rooms
            if SubjectIndex < 6:
                suitRooms = normRooms
            elif SubjectIndex == 6:
                suitRooms = sportRooms
            elif SubjectIndex == 7:
                suitRooms = itRooms
            elif SubjectIndex == 8:
                suitRooms = labRooms
            elif SubjectIndex == 9:
                suitRooms = musicRooms

            #Find suitable teachers.
            teachList = []
            for teacherObj in teacher.instlist:
                if (teacherObj.subjects[SubjectIndex] == 1 == teacherObj.years[groupObj.grade-5]): #Teach subject and grade
                    teachList.append(teacherObj)

            #If no teachers or rooms exist break code
            if len(teachList) == 0:
                sys.exit("No teacher exists with the right skillz")
            if len(suitRooms) == 0:
                sys.exit("No rooms exists with the right equipment")

            #NOW with suitable  teachers and suitable rooms and class, find a free slot that works for everyone
            for hour in range(0,hourObj): #Every hour of every subject of every class
                workCombin = np.ones((1,3))*-1
                for indexT, teacherObj in enumerate(teachList):
                    for indexR, roomObj in enumerate(suitRooms):
                        workCombin = np.vstack((workCombin,[indexT,indexR,np.count_nonzero((groupObj.busy + teacherObj.busy + roomObj.busy)== 0)]))

                maxSpots = max(workCombin[:,2])

                # if there exists a feasible solution, select randomly among all feasible ones
                if maxSpots > 0:
                    workCombin = workCombin[workCombin[:,2]==maxSpots,:]
                    ChosenSol = random.randint(0,workCombin.shape[0]-1)
                    ChosenT, ChosenR, x  = workCombin[ChosenSol,:]

                    #After selecting Room and Teacher, select the right Slot
                    ThisTeach = teachList[int(ChosenT)]
                    ThisRoom = suitRooms[int(ChosenR)]
                    RandSel = random.randint(0,int(maxSpots-1))
                    zeroArray = groupObj.busy + ThisTeach.busy + ThisRoom.busy
                    ChosenS = np.where(zeroArray == 0)[0][RandSel]#find random slot that is available
                    ChosenD = np.where(zeroArray == 0)[1][RandSel]

                #if not, select from Teachers and from Rooms and random slot
                else:
                    #print("No") - for debugging purposes
                    ChosenT = random.randint(0,(len(teacher.instlist)-1))
                    ThisTeach = teacher.instlist[int(ChosenT)]
                    ChosenR = random.randint(0,(len(room.instlist)-1))
                    ThisRoom = room.instlist[int(ChosenR)]
                    ChosenS =  random.randint(0,scheduleSlots-1)
                    ChosenD =  random.randint(0,scheduleDays-1)

                #Now make them all resources and create Vector
                ThisTeach.busy[ChosenS][ChosenD] += 1
                ThisRoom.busy[ChosenS][ChosenD] += 1
                groupObj.busy[ChosenS][ChosenD] += 1
                ScheduleItems.loc[ScheduleItems.shape[0]] = ([groupObj.Id,SubjectIndex,ChosenD,ChosenS,ThisRoom.Id,ThisTeach.first,ThisTeach.last,ThisTeach.Id])

    return ScheduleItems

def crossover(schedule1, schedule2,LL=0.0,UL=1.0):
    numObs = schedule1.shape[0]
    cutoff = np.random.uniform(LL,UL,1)[0]
    ExchangeVector = np.random.uniform(LL,UL,numObs)>cutoff
    newSched = schedule1.copy()
    for i in range(numObs):
        if ExchangeVector[i] == True:
            newSched.iloc[i,:] = schedule2.iloc[i,:]
    return newSched

def eval_Gen(currGeneration,group, teacher, room, scheduleDays,scheduleSlots):
    resArr = []
    for i in range(len(currGeneration)):
        _,_,tempScore,_ = eval_Schedule(currGeneration[i], group, teacher, room, scheduleDays,scheduleSlots)
        resArr.append(tempScore)

    return resArr

def eval_Schedule(ScheduleItems, group, teacher, room, scheduleDays,scheduleSlots):
    counter = 0
    afternoonClasses = 0
    numElem, numDetails = ScheduleItems.shape
    numGroup = len(group.instlist)
    numTeacher = len(teacher.instlist)
    numRoom = len(room.instlist)
    #Create array to track business of groups, teachers and rooms while evaluating goodness of fit
    EvalGroupArr = np.zeros((numGroup, scheduleSlots, scheduleDays))
    EvalTeacherArr = np.zeros((numTeacher, scheduleSlots, scheduleDays))
    EvalRoomArr = np.zeros((numRoom, scheduleSlots, scheduleDays))

    #Shuffle Items to evaluate in random order
    SchedItemRand = ScheduleItems#.sample(frac=1).reset_index(drop=True)
    for index, row in SchedItemRand.iterrows():
        counterRow = counter
        dayCheck = row[2]
        slotCheck = row[3]
        groupCheck = row[0]
        teacherCheck = row[7]
        roomCheck = row[4]
        subjectCheck = row[1]

        #Now evaluate schedule fit | SCORE 1-3
        if EvalGroupArr[groupCheck, slotCheck, dayCheck] == 0:
            counter += 1
        if EvalTeacherArr[teacherCheck, slotCheck, dayCheck] == 0:
            counter += 1
        if EvalRoomArr[roomCheck, slotCheck, dayCheck] == 0:
            counter += 1

        #Check if room has the right kind of equipment | SCORE 4
        if subjectCheck < 6:
            if room.instlist[roomCheck].equipment == 1:
                counter += 1
        elif subjectCheck == 6:
            if room.instlist[roomCheck].equipment == 2:
                counter += 1
        elif subjectCheck == 7:
            if room.instlist[roomCheck].equipment == 3:
                counter += 1
        elif subjectCheck == 8:
            if room.instlist[roomCheck].equipment == 4:
                counter += 1
        elif subjectCheck == 9:
            if room.instlist[roomCheck].equipment == 5:
                counter += 1

        #Check if teacher can teach subject and grade | SCORE 5
        if teacher.instlist[teacherCheck].subjects[subjectCheck] == 1:
            if teacher.instlist[teacherCheck].years[group.instlist[groupCheck].grade-5] == 1:
                counter += 1

        #Count instances of afternoon lessens to later adjust score
        if slotCheck > 5:
            afternoonClasses += 1

        #Add current score to row
        SchedItemRand.loc[index , "Score"] = (counter - counterRow)

        #Make Thing Busy
        EvalGroupArr[groupCheck, slotCheck, dayCheck] += 1
        EvalTeacherArr[teacherCheck, slotCheck, dayCheck] += 1
        EvalRoomArr[roomCheck, slotCheck, dayCheck] += 1

        #Calculate Valuation by dividing obtained score through maximum obtainable score
        valuation = counter / (numElem*5)

        ##Adjust Score
        #First calculate the number of classes that have to occur after the 6th hour
        minAfternoonClasses = 0
        for gruppe in group.instlist:
                minAfternoonClasses += max(0,sum(gruppe.hours)-scheduleDays*6)

        #Now that we have the original valuation, adjust by deducting afternoon classes of classes (every class needs a teacher - so that goes hand in hand)
        #If every class would be held in the afternoon, you'd get only 25% of the optimal score
        adjValuation = valuation * ((0.25**(1/numElem))**(afternoonClasses-minAfternoonClasses))

    return counter, valuation, adjValuation, SchedItemRand

def mutate(ScheduleItems,exploreTH, numDays, numSlots, numRooms, numTeachers):
    numRows = ScheduleItems.shape[0]
    randomNums =  np.random.rand(numRows)

    for r in range(numRows):
        if randomNums[r]<exploreTH:
            ScheduleItems.iloc[r,2] = np.random.randint(numDays) #Day
            ScheduleItems.iloc[r,3] = np.random.randint(numSlots) #Slot
            ScheduleItems.iloc[r,4] = np.random.randint(numRooms) #Room
            ScheduleItems.iloc[r,7] = np.random.randint(numTeachers) #Teacher

    return ScheduleItems

def order_Gen(schedArray,resArray):
    dummy = []
    idx   = np.argsort(resArray)[::-1]
    resArray.sort(reverse=True)

    for i in range(len(idx)):
        dummy.append(schedArray[(idx[i])])

    return dummy , resArray

#Inputs
#########################################################################
inputGroup = []
inputGroup.append([31,5, [4,4,3,3,2,2,2,2,1,1]])
inputGroup.append([28,6, [4,4,0,3,2,2,2,2,1,1]])
inputGroup.append([15,5, [4,4,3,3,2,2,2,0,0,0]])

inputTeacher = []
inputTeacher.append(["Aileen","Dreseler",[1,1],[1,1,1,1,1,1,1,1,1,1],28])
inputTeacher.append(["Berthold","Heisterkamp",[1,0],[0,0,1,1,0,0,0,0,0,0], 30])
inputTeacher.append(["Berthold","Heisterkamp2",[0,1],[0,0,1,1,0,0,0,0,0,0], 30])
inputTeacher.append(["Bernd","Stromberg",[1,1],[0,0,0,0,1,1,0,0,0,0], 30])
inputTeacher.append(["Ulf","Something",[1,1],[0,0,0,0,0,0,1,1,0,0], 30])
inputTeacher.append(["Feuer","Salamander",[1,1],[0,0,0,0,0,0,0,0,1,1], 30])

inputRoom = []
inputRoom.append([35,1])
inputRoom.append([35,1])
inputRoom.append([35,1])
inputRoom.append([35,2])
inputRoom.append([35,3])
inputRoom.append([35,4])
inputRoom.append([35,5])

#Actual Code
#########################################################################
#Create Teachers - access all via teacher.instlist
for i in range(0,len(inputTeacher)):
    currentTeacher = teacher(*inputTeacher[i], globScheduleDays, globScheduleDays)

#Create Groups
for i in range(0,len(inputGroup)):
    currentGroup = group(*inputGroup[i], globScheduleDays, globScheduleSlots)

#Create Rooms
for i in range(0,len(inputRoom)):
    currentRoom = room(*inputRoom[i],  globScheduleDays,globScheduleSlots)

############################################################################
#Create 500 Instances in Generation 0
start_time = time.time()
scheduleArray = create_NewGen(500,group, teacher, room, globScheduleDays, globScheduleSlots)
resultArray = eval_Gen(scheduleArray,group, teacher, room, globScheduleDays, globScheduleSlots)
scheduleArray, resultArray = order_Gen(scheduleArray, resultArray)
print((time.time() - start_time))

#Run for a 1000 generations
generationsCount = 1000
numKeep = 10
#Crossover number = numKeep(numKeep-1)
numKeepMutate = 5
numNew = 100
mutateTH = 0.05
qualityDevelopment = np.zeros(shape=(len(resultArray),generationsCount+1))
qualityDevelopment[:,0] = resultArray
start_time = time.time()
for i in range(generationsCount):
    scheduleArray = breed_Next(scheduleArray, numKeep,numKeepMutate, numNew, mutateTH, group,teacher, room, globScheduleDays, globScheduleSlots)
    resultArray = eval_Gen(scheduleArray,group, teacher, room, globScheduleDays, globScheduleSlots)
    scheduleArray, resultArray = order_Gen(scheduleArray, resultArray)
    qualityDevelopment[:,i+1] = resultArray
    print(str(i)+"--- %s seconds ---" % (time.time() - start_time))

pickle.dump( qualityDevelopment, open( "qualityDevelopment.p", "wb" ) )
pickle.dump( scheduleArray, open( "scheduleArray.p", "wb" ) )
pickle.dump( resultArray, open( "resultArray.p", "wb" ) )
