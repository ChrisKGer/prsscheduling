#qsub -N GeneticAlgorithm  -j Y -l m_mem_free=20G PRS_HPCC.sh
source /opt/rh/rh-python36/enable #activate Python 3
cd ~/ScheduleEnv #Switch into environment folder
source bin/activate #activate environment
cd ~/ScheduleEnv/HPCCRun #Switch to where the data is and where the output is
python3 PRS_HPCC.py #Run the python file
